# Technical Exercise
Please see the `keypad.jpeg` image in this repo, as you can see every number maps to a letter.
For example `"43556"` could spell `"hello"`

Using this number-to-letter mapping, write a function that takes a string of numbers and an array of words as arguments. The output of your function should be an array of the words that are contained "anywhere in sequence" in the string of numbers or a blank array if there are no matches.

### Examples:
----------------------------------------
INPUT:
```
(
  '45579',
  ['hello', 'world']
)
```
OUTPUT:
```
[]
```
----------------------------------------
INPUT:
```
(
  '43556',
  ['world', 'hello']
)
```
OUTPUT:
```
['hello']
```
----------------------------------------
INPUT:
```
(
  '223786597355927753757922849683',
  ['curl', 'apple', 'frog', 'cat', 'cool', 'egg', 'bat', 'den']
)
```
OUTPUT:
```
['apple', 'cat', 'bat']
```
----------------------------------------

### Rules:
 - No external libraries can be used in the solution.
 - The input string of numbers will only range from 2-9.
 - The input of words will always be in lowercase.
 - The input of words can have the same word in it multiple times, your output should also return duplciates if there are any.
 - The output needs to be an array of strings, or an empty array if there are no matches.
 - The output array must be in the same order as it was passed in the input array.

### Testing your solution
There are tests prepared in the `__tests__` directory.
Please test your solution by running `yarn test` in the terminal
If your function passes all the tests then you can try `yarn test:full` for the SERIOUS tests
The faster your function solves the full tests the better!

### Bonus points for:
  - A clean and simple to understand solution.
  - A performant solution.
  - Extra Bonus: Write a separate solution that is factored onto a single line, yes it's possible ;) (and no cheating with chaining semi-colons!).


### Definition of done:
When your function passes the `full` tests

Good luck!