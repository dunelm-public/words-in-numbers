import { wordsInNumbers } from '../wordsInNumbers'
import easyTests from './easyTests.json'

describe('wordsInNumbers', () => {
  it.each(easyTests)("str of '%p' and arr of '%p'", (str, arr, solution) => {
    expect(wordsInNumbers(str, arr)).toStrictEqual(solution)
  })
})
