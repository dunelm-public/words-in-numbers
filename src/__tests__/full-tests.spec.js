import { wordsInNumbers } from '../wordsInNumbers'
import testsOfDOOM from './testsOfDOOM.json'

describe('wordsInNumbers', () => {
  it.each(testsOfDOOM)("str of '%p' and arr of '%p'", (str, arr, solution) => {
    expect(wordsInNumbers(str, arr)).toStrictEqual(solution)
  })
})
